# Module Terraform : AZURE STORAGE ACCOUNT

## Description

Ce projet est un module Terraform qui permet de déployer un compte de stockage Azure. Il a été conçu pour être facile à utiliser et à configurer, tout en offrant la flexibilité nécessaire pour répondre à divers besoins.

## Prérequis

- Avoir installé [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- Avoir un compte [Azure](https://portal.azure.com/)
- Avoir installé [Azure CLI](https://docs.microsoft.com/fr-fr/cli/azure/install-azure-cli)
- Avoir créer un groupe de ressources ```az group create --name "nom du rg" --location "region"```

## Utilisation

Pour utiliser ce module, ajoutez le code suivant à votre fichier de configuration Terraform :

```terraform
module "storage_account" {
  source  = "git::git@gitlab.com:simplon-project/modules/storage_account.git"
  resource_group_name         = "votre_nom_de_groupe_de_ressources"
  storage_account_name        = "votre_nom_de_compte_de_stockage"
  tags                        = "vos_tags"
  storage_account_properties  = "vos_proprietes_de_compte_de_stockage"
  network_rules_properties    = "vos_regles_de_reseau"

  # options facultatives
  container_properties        = "vos_proprietes_de_conteneur"
  local_user_properties       = "vos_proprietes_d_utilisateur_local"
}
```
Puis exécuter les commandes suivantes :

```terraform
terraform init
terraform fmt
terraform validate
terraform plan
terraform apply
```
ce module créer un compte de stockage Azure, ainsi qu'un conteneur et un utilisateur local si vous le souhaitez.

# ressource terraform : AZURERM_STORAGE_ACCOUNT_LOCAL_USER

## Paramètres


- **name** : Le nom de l’utilisateur local à créer. Si vous changez ce nom, une nouvelle ressource d’utilisateur local sera créée.

- **storage_account_id** : L’ID du compte de stockage dans lequel l’utilisateur local doit être créé. Si vous changez cet ID, une nouvelle ressource d’utilisateur local sera créée.

- **ssh_key_enabled** : Si cette option est définie sur `true`, l’utilisateur peut se connecter via SSH en utilisant une clé. Par défaut, cette option est définie sur `false`.

- **ssh_password_enabled** : Si cette option est définie sur `true`, l’utilisateur peut se connecter via SSH en utilisant un mot de passe. Par défaut, cette option est définie sur `false`.

- **home_directory** : Le répertoire d’accueil de l’utilisateur.

- **ssh_authorized_key** : Ce bloc est utilisé pour spécifier les clés SSH autorisées pour l’utilisateur. Vous pouvez en avoir plusieurs. Chaque clé a une description et une key, qui est la clé publique SSH.

- **permission_scope** : Ce bloc est utilisé pour définir les permissions de l’utilisateur pour un service spécifique et une ressource spécifique dans le compte de stockage. L’utilisateur peut avoir les permissions read, create, write, delete, list pour le service blob sur la ressource spécifiée par azurerm_storage_container.main.name.

## Documentation

[source](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account_local_user)

## Authors

- [kingston](https://gitlab.com/Kingston-run)