# variables.tf

variable "subscription_id" {
  description = "ID de l'abonnement Azure."
  type        = string
  default     = ""
}
variable "resource_group_name" {
  description = "le nom du groupe de ressources"
  type        = string
}
variable "project" {
  description = "Le nom du projet."
  type        = string
  default     = ""
}
variable "owner" {
  description = "Le nom du propriétaire."
  type        = string
  default     = ""
}
# Variables pour le compte de stockage

variable "storage_account_name" {
  description = "Le nom du compte de stockage."
  type        = string
  default     = "stoa1"

  validation {
    condition     = length(var.storage_account_name) >= 1 && length(var.storage_account_name) <= 24 && var.storage_account_name == lower(var.storage_account_name)
    error_message = "Le nom du compte de stockage doit avoir entre 1 et 24 caractères et être en minuscules."
  }
}
variable "storage_account_properties" {
  description = "Les propriétés du compte de stockage"
  type = object({
    account_kind                    = string
    account_tier                    = string
    account_replication_type        = string
    min_tls_version                 = string
    access_tier                     = string
    enable_https_traffic_only       = bool
    allow_nested_items_to_be_public = bool
    shared_access_key_enabled       = bool
    public_network_access_enabled   = bool
    is_hns_enabled                  = bool
    sftp_enabled                    = bool
    network_rules_properties = object({
      default_action             = string
      ip_rules                   = list(string)
      virtual_network_subnet_ids = list(string)
      bypass                     = list(string)
      private_link_access = object({
        endpoint_resource_id = optional(string)
        endpoint_tenant_id   = optional(string)
      })
    })
  })
  default = {
    account_kind                    = "StorageV2"
    account_tier                    = "Standard"
    account_replication_type        = "LRS"
    min_tls_version                 = "TLS1_2"
    access_tier                     = "Hot"
    enable_https_traffic_only       = true
    allow_nested_items_to_be_public = true
    shared_access_key_enabled       = true
    public_network_access_enabled   = true
    is_hns_enabled                  = true
    sftp_enabled                    = true
    network_rules_properties = {
      default_action             = "Deny"
      bypass                     = ["AzureServices", "Metrics"]
      ip_rules                   = []
      virtual_network_subnet_ids = []
      private_link_access = {
        endpoint_resource_id = ""
        endpoint_tenant_id   = ""
      }
    }
  }
}

# Variables pour le conteneur de stockage
variable "container_properties" {
  description = "Les propriétés du conteneur. 'container_access_type' peut être 'blob', 'container', 'private' ou 'off'."
  type = object({
    container_names       = list(string)
    container_access_type = string
  })
  default = {
    container_names       = ["container"]
    container_access_type = "private"
  }
}

# Variables pour l'utilisateur local du compte de stockage
variable "local_user_properties" {
  description = "Les propriétés de l'utilisateur local"
  type = object({
    local_user_name      = string
    storage_account_id   = string
    ssh_key_enabled      = bool
    ssh_password_enabled = bool
    home_directory       = string
    permission_scope = object({
      permissions = object({
        read   = bool
        list   = bool
        delete = bool
        create = bool
        write  = bool
      })
      #resource_name = string
      service = string
    })
  })
  default = {
    local_user_name      = "user1"
    storage_account_id   = ""
    ssh_key_enabled      = true
    ssh_password_enabled = false
    home_directory       = "/home/user1"
    permission_scope = {
      permissions = {
        read   = true
        list   = true
        delete = true
        create = true
        write  = true
      }
      service = "blob"
    }
  }
}

# # Variables pour le Key Vault
variable "key_vault_name" {
  description = "The name of the Key Vault"
  type        = string
  default     = ""
}
variable "content_type" {
  description = "The content type of the secret"
  type        = string
  default     = "compte de stockage/ssh-key"
}
variable "expiration_date" {
  description = "The expiration date of the secret"
  type        = string
  default     = "2030-01-01T01:02:03Z"
}




variable "ssh_keys" {
  type = list(object({
    name      = string
    algorithm = optional(string)
    rsa_bits  = optional(number)
  }))
  default = [
    {
      name      = "key1"
      algorithm = "RSA"
      rsa_bits  = "4096"
    }
  ]
}
