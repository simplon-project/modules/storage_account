locals {
  timestamp           = timestamp()
  day                 = formatdate("DD", local.timestamp)
  month               = formatdate("MMMM", local.timestamp)
  year                = formatdate("YY", local.timestamp)
  time                = replace(formatdate("HH:mm", local.timestamp), ":", "h")
  timestamp_formatted = "${local.day}${lower(local.month)}${local.year}${local.time}"

  key_vault_exists = length(data.azurerm_key_vault.existing) > 0 ? true : false
  public_key1      = module.ssh_keys.public_keys_openssh["key1"]

  tags = {
    project = var.project
    owner   = var.owner
    date    = local.timestamp_formatted
  }
}
module "ssh_keys" {
  source = "git::git@gitlab.com:simplon-project/modules/tls_private_key.git"
  keys   = var.ssh_keys
}
resource "random_string" "resource_code" {
  length      = max(1, 24 - length(lower(replace(var.storage_account_name, "_", ""))))
  numeric     = true
  min_numeric = 5
  special     = false
  upper       = false
  lower       = true
}
resource "azurerm_storage_account" "main" {
  name                            = "${lower(replace(var.storage_account_name, "_", ""))}${random_string.resource_code.result}"
  resource_group_name             = data.azurerm_resource_group.main.name
  location                        = data.azurerm_resource_group.main.location
  account_kind                    = var.storage_account_properties.account_kind
  account_tier                    = var.storage_account_properties.account_tier
  account_replication_type        = var.storage_account_properties.account_replication_type
  min_tls_version                 = var.storage_account_properties.min_tls_version
  access_tier                     = var.storage_account_properties.access_tier
  enable_https_traffic_only       = var.storage_account_properties.enable_https_traffic_only
  allow_nested_items_to_be_public = var.storage_account_properties.allow_nested_items_to_be_public
  shared_access_key_enabled       = var.storage_account_properties.shared_access_key_enabled
  public_network_access_enabled   = var.storage_account_properties.public_network_access_enabled
  is_hns_enabled                  = var.storage_account_properties.is_hns_enabled
  sftp_enabled                    = var.storage_account_properties.sftp_enabled

  dynamic "network_rules" {
    for_each = var.storage_account_properties.network_rules_properties != null ? [1] : []
    content {
      default_action             = var.storage_account_properties.network_rules_properties.default_action
      bypass                     = var.storage_account_properties.network_rules_properties.bypass
      ip_rules                   = var.storage_account_properties.network_rules_properties.ip_rules
      virtual_network_subnet_ids = var.storage_account_properties.network_rules_properties.virtual_network_subnet_ids
      dynamic "private_link_access" {
        for_each = var.storage_account_properties.network_rules_properties.private_link_access != null && var.storage_account_properties.network_rules_properties.private_link_access.endpoint_resource_id != "" && var.storage_account_properties.network_rules_properties.private_link_access.endpoint_tenant_id != "" ? [1] : []
        content {
          endpoint_resource_id = var.storage_account_properties.network_rules_properties.private_link_access.endpoint_resource_id
          endpoint_tenant_id   = var.storage_account_properties.network_rules_properties.private_link_access.endpoint_tenant_id
        }
      }
    }
  }
  lifecycle {
    ignore_changes = [
      name,
    ]
  }
  tags = local.tags
}
resource "azurerm_storage_container" "main" {
  for_each = toset(var.container_properties.container_names)

  name                  = "${each.value}${local.timestamp_formatted}"
  storage_account_name  = data.azurerm_storage_account.main.name
  container_access_type = var.container_properties.container_access_type
  metadata              = local.tags

  lifecycle {
    ignore_changes = [
      name,
    ]
  }
}
resource "azurerm_key_vault_secret" "storage_account_key" {
  count           = local.key_vault_exists ? 1 : 0
  name            = "storage-account-key"
  value           = data.azurerm_storage_account_sas.sas.sas
  key_vault_id    = data.azurerm_key_vault.existing[0].id
  content_type    = "sas"
  expiration_date = var.expiration_date
  tags            = local.tags
}
resource "azurerm_key_vault_secret" "ssh_keys" {
  count           = local.key_vault_exists ? 1 : 0
  name            = "ssh-key"
  value           = module.ssh_keys.public_keys_openssh["key1"]
  key_vault_id    = data.azurerm_key_vault.existing[0].id
  content_type    = var.content_type
  expiration_date = var.expiration_date
}
resource "local_file" "private_ssh_key" {
  count           = local.key_vault_exists ? 0 : 1
  content         = module.ssh_keys.private_keys_pem["key1"]
  filename        = "${path.root}/ssh/key.pem"
  file_permission = "0600"
}
resource "azurerm_storage_account_local_user" "user" {
  name                 = var.local_user_properties.local_user_name
  storage_account_id   = data.azurerm_storage_account.main.id
  ssh_key_enabled      = var.local_user_properties.ssh_key_enabled
  ssh_password_enabled = var.local_user_properties.ssh_password_enabled
  home_directory       = var.local_user_properties.home_directory
  ssh_authorized_key {
    description = "key1"
    key         = local.public_key1
  }
  permission_scope {
    permissions {
      read   = var.local_user_properties.permission_scope.permissions.read
      list   = var.local_user_properties.permission_scope.permissions.list
      delete = var.local_user_properties.permission_scope.permissions.delete
      create = var.local_user_properties.permission_scope.permissions.create
      write  = var.local_user_properties.permission_scope.permissions.write
    }
    resource_name = data.azurerm_storage_container.main["container"].name
    service       = var.local_user_properties.permission_scope.service
  }
  depends_on = [
    azurerm_storage_account.main,
    azurerm_storage_container.main,
    module.ssh_keys
  ]
}


