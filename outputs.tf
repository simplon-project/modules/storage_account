output "resource_group_name" {
  value = data.azurerm_resource_group.main.name
}

output "location" {
  value = data.azurerm_resource_group.main.location
}


output "subscription_id" {
  description = "The subscription ID of the Azure client"
  value       = data.azurerm_client_config.current.subscription_id
}

output "tenant_id" {
  description = "The tenant ID of the Azure client"
  value       = data.azurerm_client_config.current.tenant_id
}

output "client_id" {
  description = "The client ID of the Azure client"
  value       = data.azurerm_client_config.current.client_id
}

output "object_id" {
  description = "The object ID of the Azure client"
  value       = data.azurerm_client_config.current.object_id
}


output "storage_account_id" {
  description = "L'ID du compte de stockage"
  value       = azurerm_storage_account.main.id
}

output "storage_account_name" {
  description = "Le nom du compte de stockage"
  value       = azurerm_storage_account.main.name
}

output "storage_account_primary_access_key" {
  description = "La clé d'accès primaire du compte de stockage"
  value       = data.azurerm_storage_account.main.primary_access_key
  sensitive   = true
}

output "storage_account_container_names" {
  value       = [for c in data.azurerm_storage_container.main : c.name]
  description = "Noms des conteneurs de stockage"
}


output "local_user_name" {
  description = "Le nom de l'utilisateur local"
  value       = azurerm_storage_account_local_user.user.name
}

output "local_user_id" {
  description = "L'ID de l'utilisateur local"
  value       = azurerm_storage_account_local_user.user.id
}

output "local_user_ssh_authorized_key" {
  description = "La clé SSH autorisée de l'utilisateur local"
  value       = length(azurerm_storage_account_local_user.user.ssh_authorized_key) > 0 ? azurerm_storage_account_local_user.user.ssh_authorized_key[0] : null
  sensitive   = true
}

output "local_user_permission_scope" {
  description = "La portée de permission de l'utilisateur local"
  value       = azurerm_storage_account_local_user.user.permission_scope[0]
}

output "public_keys" {
  value = module.ssh_keys.public_keys_openssh
}
output "sas_url_query_string" {
  value     = data.azurerm_storage_account_sas.sas.sas
  sensitive = true
}
