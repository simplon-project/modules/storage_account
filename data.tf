data "azurerm_client_config" "current" {}

data "azurerm_resource_group" "main" {
  name = var.resource_group_name
}
data "azurerm_storage_account" "main" {
  name                = azurerm_storage_account.main.name
  resource_group_name = azurerm_storage_account.main.resource_group_name
}
data "azurerm_storage_container" "main" {
  for_each = azurerm_storage_container.main

  name                 = each.value.name
  storage_account_name = data.azurerm_storage_account.main.name
}
data "azurerm_key_vault" "existing" {
  count               = var.key_vault_name != "" ? 1 : 0
  name                = var.key_vault_name
  resource_group_name = data.azurerm_resource_group.main.name
}
data "azurerm_storage_account_sas" "sas" {
  connection_string = azurerm_storage_account.main.primary_connection_string
  https_only        = false
  resource_types {
    service   = true
    container = false
    object    = false
  }
  services {
    blob  = true
    queue = true
    table = true
    file  = true
  }
  start  = "2024-01-01"
  expiry = "2030-01-01"
  permissions {
    read    = true
    write   = true
    delete  = true
    list    = true
    add     = true
    create  = true
    update  = true
    process = true
    tag     = true
    filter  = true
  }
}